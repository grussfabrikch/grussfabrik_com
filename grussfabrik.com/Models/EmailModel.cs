﻿using System.ComponentModel.DataAnnotations;

namespace grussfabrik.Models
{
    public class EmailModel
    {
        [Required]
        [Display(Name = "Name")]
        public string FullName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string UserEmail { get; set; }

        [Required]
        [Display(Name = "Message")]
        public string Body { get; set; }
    }
}
