﻿using grussfabrik.Models;
using System;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;

namespace grussfabrik.Controllers
{
    public class ContactUsController : Controller
    {

        public JsonResult SendEMail(EmailModel model)
        {
            //var mailSever = "smtp.gmail.com";
            //var mailSeverPort = 587;
            //var senderMailAddress = "any@gmail.com";
            //var adminMail = "satyanamsoft@gmail.com";
            //var userId = "satyanamsoft@gmail.com";

            var mailSever = "email-smtp.eu-west-1.amazonaws.com";
            var senderMailAddress = "info@kugelschreiberdruck.de";
            var adminMail = "info@kugelschreiberdruck.de";
            var userId = "AKIAJ47HFFDQRZVTSEJQ";

            var password = string.Empty;
            bool isMailSent = false;
            string erorrMessage = string.Empty;
            if (ModelState.IsValid)
            {
                try
                {
                    //var client = new SmtpClient(mailSever, mailSeverPort);
                    var client = new SmtpClient(mailSever);
                    client.Credentials = new NetworkCredential(userId, password);
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(senderMailAddress);
                    mailMessage.To.Add(adminMail);
                    mailMessage.Body = model.Body;
                    client.Send(mailMessage);
                }
                catch (Exception ex)
                {
                    isMailSent = false;
                    erorrMessage = $"Failed sending mail: {ex.Message}";
                }
            }
            return Json(new { success = isMailSent, erorr = erorrMessage });
        }

    }
}