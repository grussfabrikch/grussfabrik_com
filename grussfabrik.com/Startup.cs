﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(grussfabrik.Startup))]
namespace grussfabrik
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
